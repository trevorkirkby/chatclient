#include <stdio.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

int sock_connect(char *host, int port) {
  int sockfd, n, errno;

  struct sockaddr_in server_addr;
  int addr_len = sizeof(server_addr);
  memset(&server_addr, '0', addr_len);
  server_addr.sin_family = AF_INET;
  server_addr.sin_port = htons(port);

  int init = inet_pton(AF_INET, host, &server_addr.sin_addr);
  if (init < 1) {
    printf("Inet_pton failed with code %d.\n", init);
    return -1;
  }

  sockfd = socket(AF_INET,SOCK_STREAM,0);
  if (sockfd == -1) {
    printf("Cannot create socket.\n");
    return -1;
  }

  if (connect(sockfd, (struct sockaddr *)&server_addr, addr_len) == -1) {
    printf("Cannot connect.\n");
    return -1;
  }

  struct timeval timev; //Credits to the class powerpoint for code that creates a timeout.
  timev.tv_sec = 0;
  timev.tv_usec = 1000 * 500;
  setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, &timev, sizeof(timev));

  return sockfd;
}
