/*
A rudimentary chat client to explore socket protocols in C.
by Trevor Kirkby
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <curses.h>
#include <pthread.h>
#include "sock_connect.h"
#include "sock_io.h"

#define HOST "10.115.20.250" //AKA pilot.westmont.edu
#define PORT 49153

int main(int argc, char *argv[]) {
  char *uname = argv[1];
  if (argc < 2) {
    printf("Please specify a username as an argument.\n");
    return 3;
  }

  int sockfd = sock_connect(HOST, PORT);
  if (sockfd == -1) {
    return 1; //We should already have recieved a more specific error message from sock_connect(), so just exit the program with a nonzero return code.
  }

  initscr(); //The proper function calls to initialzie ncurses are explained here: http://www.cs.ukzn.ac.za/~hughm/os/notes/ncurses.html#window
  cbreak();  //I am not making use of noecho() since in this case the user should be able to see what they have typed reflected on the screen.
  clear();
  WINDOW *win1 = newwin(LINES-5, COLS, 0, 0);
  WINDOW *win2 = newwin(4, COLS, LINES-4, 0);
  scrollok(win1, 1);
  scrollok(win2, 1); //Automatically handles scrolling in each window individually.

  char *login = malloc(strlen(uname)+1);
  strcat(login, uname);
  strcat(login, "\n");
  writesock(sockfd, login);
  free(login);
  wprintw(win1, "Connected successfully as %s.\n\n", uname);
  wrefresh(win1);

  pthread_t thread_id; //A basic guide on threading in C is detailed here https://www.geeksforgeeks.org/multithreading-c-2/
  recv_args args;      //Create a struct to pass both sockfd and ncurses window as a single argument.
  args.sockfd = sockfd;
  args.win = win1;
  pthread_create(&thread_id, NULL, recv_and_print, &args); //Start up our thread to read the socket. Since one thread only reads the socket and the other only writes to the socket, no locking is required.

  char sendbuffer[256];
  while(1) {
    memset(&sendbuffer, '\0', 256); //Zero out the buffer.
    wprintw(win2, "%s> ", uname);
    wrefresh(win2);
    wgetnstr(win2, sendbuffer, 254);
    strcat(sendbuffer, "\n"); //Wgetnstr does not include the delimiting newline, so we add one back in.
    if(sendbuffer[0] != '\n') { //Send a message only if the user actually typed something before pressing enter.
      writesock(sockfd, sendbuffer);
    }
  }
  close(sockfd);
  endwin();
  return 0;
}
