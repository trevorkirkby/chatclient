#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <curses.h>

#define BUFFER 32 //Recieve data in 32 byte chunks.

typedef struct {
  int sockfd;
  WINDOW *win;
} recv_args;

void *recv_and_print(void *args_void_pointer) { //Meant to be run as a thread that constantly updates the screen with incoming messages.
  recv_args *args_pointer = args_void_pointer;  //Accepts a pointer to a Recvdata struct containing the window to write to and the socket file descriptor to read from.
  recv_args args = *args_pointer;               //Could probably do this in one line by recasting the pointer and dereferencing it, but that would be messy.
  int sockfd = args.sockfd;
  WINDOW *win = args.win;
  char recvbuffer[BUFFER];
  while (1) {
    memset(&recvbuffer, '\0', BUFFER); //Zero out the buffer.
    if (recv(sockfd, recvbuffer, BUFFER, 0) >= 1) { //If we recieved anything, show it on the screen.
      wprintw(win, "%s", recvbuffer);
      wrefresh(win);
    }
  }
}

void writesock(int sockfd, char *data) {
  send(sockfd, data, strlen(data), 0);
}
